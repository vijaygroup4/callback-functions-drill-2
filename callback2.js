//importing both fs and path modules
const fs = require("fs");
const path = require("path");

//function that return lists by id
function problem2(id, callback) {
  //stopping the execution for 2 sec
  setTimeout(() => {
    let listsFilePath = path.join(__dirname, "lists_1.json");
    //reading the lists json file
    fs.readFile(listsFilePath, "utf-8", (error, objectData) => {
      if (error) {
        return callback(error);
      }

      try {
        let object = JSON.parse(objectData);
        //getting the list
        let list = object[id];
        callback(null, list);
      } catch (error) {
        console.log(error);
      }
    });
  }, 2000);
}

//exporting the above function
module.exports = problem2;
