//importing fs and path modules
const fs = require("fs");
const path = require("path");

//function that returns board details by id
function problem1(id, callback) {
  //stopping the execution for 2 sec
  setTimeout(() => {
    let boardsFilePath = path.join(__dirname, "boards_1.json");
    //reading the boards json file
    fs.readFile(boardsFilePath, "utf-8", (error, boardsData) => {
      if (error) {
        return callback(error);
      }

      try {
        let boards = JSON.parse(boardsData);
        //finding the board
        let board = boards.find((board) => {
          return board.id === id;
        });

        callback(null, board);
      } catch (error) {
        console.log(error);
      }
    });
  }, 2000);
}

//exporting the above function
module.exports = problem1;
