//importing both fs and path modules
const fs = require("fs");
const path = require("path");

//importing problem2 and problem3 functions
const problem1 = require("./callback1");
const problem2 = require("./callback2");
const problem3 = require("./callback3");

//function that performs:
//Get information from the Thanos boards
//Get all the lists for the Thanos board
//Get all cards for the Mind list simultaneously
function problem4(callback) {
  //stopping the execution for 2sec
  setTimeout(() => {
    let boardsFilePath = path.join(__dirname, "boards_1.json");
    //reading the boards file
    fs.readFile(boardsFilePath, "utf-8", (boardsError, boardsData) => {
      if (boardsError) {
        return callback(boardsError);
      }

      try {
        let boards = JSON.parse(boardsData);
        //filtering for Thanos id
        let filteredBoards = boards.filter((board) => {
          return board.name === "Thanos";
        });
        let boardId = filteredBoards[0].id;

        //getting the Thanos information
        problem1(boardId, (error, board) => {
          if (error) {
            console.log(error);
          } else if (board) {
            console.log("Thanos Details:", board);
          } else {
            console.log("Board not found");
          }
        });

        //using previously defined problem2 function for getting lists
        problem2(boardId, (error, list) => {
          if (error) {
            console.log(error);
          } else if (list) {
            console.log("list=", list);
          } else {
            console.log("No list found");
          }
        });

        let listsFilePath = path.join(__dirname, "lists_1.json");
        //reading the lists file
        fs.readFile(listsFilePath, "utf-8", (listsError, listsData) => {
          if (listsError) {
            return callback(listsError);
          }

          let listsObject = JSON.parse(listsData);
          let keysArray = Object.keys(listsObject);

          let requiredId = "";
          //filtering for Mind id
          keysArray.forEach((innerId) => {
            let innerArray = listsObject[innerId];
            innerArray.forEach((object) => {
              if (object.name === "Mind") {
                requiredId = object.id;
              }
            });
          });

          //using previously defined problem3 function for getting cards
          problem3(requiredId, (error, cards) => {
            if (error) {
              return callback(error);
            } else if (cards) {
              console.log("cards=", cards);
            } else {
              console.log("cards not found");
            }
          });
        });
      } catch (error) {
        console.log(error);
      }
    });
  }, 2000);
}

//exporting the above function
module.exports = problem4;
