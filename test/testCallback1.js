//import problem1 function
const problem1 = require("../callback1");

let boardId = "abc122dc";
//executing the problem1 function
problem1(boardId, (error, board) => {
  if (error) {
    console.log(error);
  } else if (board) {
    console.log("Board Details:", board);
  } else {
    console.log("Board not found");
  }
});
