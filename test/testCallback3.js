//importing problem3 function
const problem3 = require("../callback3");

let listId = "qwsa221";
//executing problem3 function
problem3(listId, (error, cards) => {
  if (error) {
    console.log(error);
  } else if (cards) {
    console.log(cards);
  } else {
    console.log("No list found");
  }
});
