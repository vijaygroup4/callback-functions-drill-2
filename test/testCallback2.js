//importing problem2 function
const problem2 = require("../callback2");

let boardId = "abc122dc";
//executing problem2 function
problem2(boardId, (error, list) => {
  if (error) {
    console.log(error);
  } else if (list) {
    console.log(list);
  } else {
    console.log("No list found");
  }
});
