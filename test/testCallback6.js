//importing problem6 function
const problem6 = require("../callback6");

//executing problem6 function
problem6((error) => {
  if (error) {
    console.log(error);
  }
});
