//importing problem5 function
const problem5 = require("../callback5");

//executing problem5 function
problem5((error) => {
  if (error) {
    console.log(error);
  }
});
