//importing problem4 function
const problem4 = require("../callback4");

//executing problem4 function
problem4((error) => {
  if (error) {
    console.log(error);
  }
});
